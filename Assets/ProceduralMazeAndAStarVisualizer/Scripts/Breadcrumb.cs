﻿
/*! 
 * \file
 * \author Stig Olavsen <stig.olavsen@freakshowstudio.com>
 * \author http://www.freakshowstudio.com
 * \date © 2016
 * \copyright Unlicense <http://unlicense.org/>
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Maze
{
    /// <summary>
    /// This is a class that represents a breadcrumb set by the A* 
    /// algorithm.
    /// 
    /// The breadcrumb is dropped on every visited node, and set to 
    /// display three numbers, representing the calculated distance cost
    /// of the node, the heuristic cost, and the total cost, which is the
    /// sum of the distance and heuristic costs.
    /// 
    /// This lets you see how the calulations of the A* algorithm are made,
    /// and shows why it selects a given node.
    /// 
    /// This script should be attached to a prefab, that is then set as
    /// the Breadcrumb Prefab on the MazeDrawer class.
    /// </summary>
    public class Breadcrumb : MonoBehaviour 
    {
        #region Inspector Variables
        /// <summary>
        /// A reference to the TextMesh that should display the distance cost.
        /// </summary>
        [SerializeField] private TextMesh _distanceCostText;
        /// <summary>
        /// A reference to the TextMesh that should display the heuristic cost.
        /// </summary>
        [SerializeField] private TextMesh _heuristicCostText;
        /// <summary>
        /// A reference to the TextMesh that should display the total cost.
        /// </summary>
        [SerializeField] private TextMesh _totalCostText;
        #endregion // Inspector Variables

        #region Public Methods
        /// <summary>
        /// Updates the TextMesh fields with the specified costs.
        /// </summary>
        /// <param name="distanceCost">Distance cost.</param>
        /// <param name="heuristicCost">Heuristic cost.</param>
        /// <param name="totalCost">Total cost.</param>
        public void SetCost(
            float distanceCost, 
            float heuristicCost, 
            float totalCost)
        {
            _distanceCostText.text = distanceCost.ToString("F1");
            _heuristicCostText.text = heuristicCost.ToString("F1");
            _totalCostText.text = totalCost.ToString("F1");
        }
        #endregion // Public Methods
	}
}
