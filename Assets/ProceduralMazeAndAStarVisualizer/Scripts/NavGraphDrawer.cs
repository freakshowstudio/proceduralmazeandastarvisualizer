﻿
/*! 
 * \file
 * \author Stig Olavsen <stig.olavsen@freakshowstudio.com>
 * \author http://www.freakshowstudio.com
 * \date © 2016
 * \copyright Unlicense <http://unlicense.org/>
 */

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Maze
{
    /// <summary>
    /// A class that will handle drawing the navigation graph.
    /// 
    /// When the MazeGenerator generates a maze, it will simultaneosly
    /// generate a navigation graph, that is used by the NavSolver to find
    /// a path through the maze.
    /// 
    /// This class is used to draw the navigation graph after the 
    /// MazeGenerator has finised generating the maze. 
    /// 
    /// It does this by creating a number of LineRenderer objects that
    /// are made to look as arrows.
    /// </summary>
    public class NavGraphDrawer
    {
        #region Private Variables
        /// <summary>
        /// The Z position of the navigation graph.
        /// </summary>
        private float _height;
        /// <summary>
        /// The start width of the LineRenderer.
        /// </summary>
        private float _startWidth;
        /// <summary>
        /// The end width of the LineRenderer.
        /// </summary>
        private float _endWidth;
        /// <summary>
        /// The layer that the navigation graph objects should use.
        /// </summary>
        private int _layer;
        /// <summary>
        /// The material used by the LineRenderer lines.
        /// </summary>
        private Material _material;
        /// <summary>
        /// A list that keeps track of the drawn lines. Lets us go back
        /// and destroy the lines when needed.
        /// </summary>
        private List<GameObject> _edges;
        /// <summary>
        /// A Transform that the lines should be parented to.
        /// </summary>
        private Transform _parent;
        #endregion // Private Variables

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the NavGraphDrawer class.
        /// </summary>
        /// <param name="startWidth">Start width.</param>
        /// <param name="endWidth">End width.</param>
        /// <param name="material">Line material.</param>
        /// <param name="height">Height.</param>
        /// <param name="layer">Layer.</param>
        /// <param name="parent">Parent.</param>
        public NavGraphDrawer(
            float startWidth, float endWidth, 
            Material material, 
            float height = 0f, 
            int layer = 0,
            Transform parent = null)
        {
            _startWidth = startWidth;
            _endWidth = endWidth;
            _material = material;
            _height = height;
            _layer = layer;
            _edges = new List<GameObject>();
            if (parent == null)
            {
                GameObject parentGO = new GameObject("Navigation Graph");
                parent = parentGO.transform;
            }
            _parent = parent;
        }
        #endregion // Constructors

        #region Public Methods
        /// <summary>
        /// Draws a line between NavNode a and b.
        /// </summary>
        /// <param name="from">From.</param>
        /// <param name="to">To.</param>
        public void DrawNavEdge(NavNode from, NavNode to)
        {
            Vector3 startPosition = 
                new Vector3(from.PositionX, from.PositionY, _height);
            Vector3 endPosition = 
                new Vector3(to.PositionX, to.PositionY, _height);
            Vector3 halfwayPosition = 
                startPosition + ((endPosition - startPosition) * 0.5f);
                
            string edgeName = 
                string.Format("Nav Edge from {0} to {1}", from, to);

            GameObject edgeObject = new GameObject(edgeName);
            edgeObject.transform.SetParent(_parent);
            edgeObject.layer = _layer;

            LineRenderer edgeLine = edgeObject.AddComponent<LineRenderer>();
            edgeLine.SetPositions(
                new Vector3[] { startPosition, halfwayPosition });
            edgeLine.startWidth = _startWidth;
            edgeLine.endWidth = _endWidth;
            edgeLine.material = _material;

            _edges.Add(edgeObject);
        }

        /// <summary>
        /// Destroys all the drawn lines.
        /// </summary>
        public void Clear()
        {
            foreach (GameObject edge in _edges)
            {
                GameObject.Destroy(edge);
            }
            _edges.Clear();
        }
        #endregion // Public Methods
    }
}

