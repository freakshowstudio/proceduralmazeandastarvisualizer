﻿
/*! 
 * \file
 * \author Stig Olavsen <stig.olavsen@freakshowstudio.com>
 * \author http://www.freakshowstudio.com
 * \date © 2016
 * \copyright Unlicense <http://unlicense.org/>
 */

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Maze
{
    /// <summary>
    /// This class represents a single node in the navigation graph.
    /// 
    /// The navigation graph is created by the MazeGenerator, and is 
    /// used by the NavSolver to find a path through the maze.
    /// </summary>
    public class NavNode : IComparable<NavNode>, IEquatable<NavNode>
    {
        #region Public Properties
        /// <summary>
        /// Gets or sets the parent NavNode. This is the node that leads
        /// to this, when solving for a path.
        /// </summary>
        /// <value>The parent.</value>
        public NavNode Parent { get; set; }
        /// <summary>
        /// Gets or sets the distance cost of this NavNode.
        /// </summary>
        /// <value>The distance cost.</value>
        public float DistanceCost { get; set; }
        /// <summary>
        /// Gets or sets the heuristic cost of this NavNode.
        /// </summary>
        /// <value>The heuristic cost.</value>
        public float HeuristicCost { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether this NavNode is closed.
        /// </summary>
        /// <value><c>true</c> if closed; otherwise, <c>false</c>.</value>
        public bool Closed { get; set; }
        /// <summary>
        /// Gets the edges of this node. These are the neighbouring nodes
        /// in the navigation graph.
        /// </summary>
        /// <value>The edges.</value>
        public List<NavEdge> Edges { get; private set; }
        /// <summary>
        /// Gets or sets the x position of this NavNode.
        /// </summary>
        /// <value>The x position.</value>
        public int PositionX { get; set; }
        /// <summary>
        /// Gets or sets the y position of this NavNode.
        /// </summary>
        /// <value>The y position.</value>
        public int PositionY { get; set; }
        /// <summary>
        /// Gets the total cost of this NavNode.
        /// </summary>
        /// <value>The total cost.</value>
        public float TotalCost
        {
            get { return DistanceCost + HeuristicCost; }
        }
        #endregion // Properties

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the NavNode class.
        /// </summary>
        public NavNode() : this(null, 0, 0) {}

        /// <summary>
        /// Initializes a new instance of the NavNode class.
        /// </summary>
        /// <param name="parent">Parent.</param>
        /// <param name="distanceCost">Distance cost.</param>
        /// <param name="heuristicCost">Heuristic cost.</param>
        public NavNode(
            NavNode parent, 
            float distanceCost, 
            float heuristicCost)
        {
            Parent = parent;
            DistanceCost = distanceCost;
            HeuristicCost = heuristicCost;
            Closed = false;
            Edges = new List<NavEdge>();
        }
        #endregion // Constructors

        #region IComparable Implementation
        /// <summary>
        /// Compares this NavNode to another NavNode. 
        /// 
        /// This is used by the NavSolver to compare the total distance cost
        /// of one node to another, to ensure we are taking the shortest path.
        /// 
        /// The nodes are sorted by this function in the BinaryHeap used by
        /// the NavSolver.
        /// </summary>
        /// <returns>The comparison value.</returns>
        /// <param name="other">The other NavNode.</param>
        public int CompareTo(NavNode other)
        {
            return other.TotalCost.CompareTo(TotalCost);
        }
        #endregion // IComparable Implementation

        #region IEquatable Implementation
        /// <summary>
        /// Determines whether the specified NavNode is equal to 
        /// the current NavNode.
        /// 
        /// Two NavNodes are considered equal if their X and Y position
        /// are the same.
        /// </summary>
        /// <param name="other">The NavNode to compare with the 
        /// current NavNode.</param>
        /// <returns><c>true</c> if the specified NavNode is equal to 
        /// the current NavNode; otherwise, <c>false</c>.</returns>
        public bool Equals(NavNode other)
        {
            return 
                (this.PositionX == other.PositionX) &&
                (this.PositionY == other.PositionY);
        }
        #endregion // IEquatable Implementation

        #region Public Methods
        /// <summary>
        /// Returns a String that represents the current NavNode.
        /// </summary>
        /// <returns>A String that represents the current NavNode.</returns>
        public override string ToString()
        {
            return string.Format(
                "[NavNode: {0},{1}]", 
                PositionX, PositionY);
        }
        #endregion // Public Methods
    }
}

