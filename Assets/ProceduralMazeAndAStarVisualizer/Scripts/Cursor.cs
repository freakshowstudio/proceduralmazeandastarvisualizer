﻿
/*! 
 * \file
 * \author Stig Olavsen <stig.olavsen@freakshowstudio.com>
 * \author http://www.freakshowstudio.com
 * \date © 2016
 * \copyright Unlicense <http://unlicense.org/>
 */

using System;
using UnityEngine;
using UnityEngine.Assertions;

namespace Maze
{
    /// <summary>
    /// A class representing a cursor that is moved by the maze generation
    /// and A* algorithms to show the currently processed node.
    /// 
    /// As the algorithms work, the cursor is updated to show the 
    /// node currently being processed, and the color is updated to show
    /// the status of the algorithm at this point.
    /// 
    /// This script should be attached on a prefab that is then set as
    /// the Cursor Prefab of the MazeDrawer class.
    /// </summary>
    public class Cursor : MonoBehaviour
    {
        #region Inspector variables
        /// <summary>
        /// The color of the cursor while the algorithm is idle.
        /// </summary>
        [SerializeField] private Color _idleColor = Color.gray;
        /// <summary>
        /// The color of the cursor while the algorithm is searching.
        /// </summary>
        [SerializeField] private Color _searchingColor = Color.yellow;
        /// <summary>
        /// The color of the cursor while the algorithm is updating.
        /// </summary>
        [SerializeField] private Color _updatingColor = Color.red;
        #endregion // Inspector variables

        #region Enums
        /// <summary>
        /// An enum representing the possible states of the algorithm.
        /// </summary>
        public enum Status
        {
            Idle,
            Searching,
            Updating,
        }
        #endregion // Enums

        #region Public Methods
        /// <summary>
        /// Sets the position of the cursor.
        /// </summary>
        /// <param name="position">Position.</param>
        public void SetPosition(Vector3 position)
        {
            transform.position = position;
        }

        /// <summary>
        /// Sets the status of the cursor. This will update its color.
        /// </summary>
        /// <param name="status">Status.</param>
        public void SetStatus(Status status)
        {
            Renderer renderer = GetComponent<Renderer>();
            Assert.IsNotNull(renderer);
            switch (status)
            {
                case Status.Idle:
                    renderer.material.color = _idleColor;
                    break;
                case Status.Searching:
                    renderer.material.color = _searchingColor;
                    break;
                case Status.Updating:
                    renderer.material.color = _updatingColor;
                    break;
            }
        }
        #endregion // Public Methods
    }
}

