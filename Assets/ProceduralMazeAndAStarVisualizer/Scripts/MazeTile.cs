﻿
/*! 
 * \file
 * \author Stig Olavsen <stig.olavsen@freakshowstudio.com>
 * \author http://www.freakshowstudio.com
 * \date © 2016
 * \copyright Unlicense <http://unlicense.org/>
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Maze
{
    /// <summary>
    /// A class to represent one tile in the maze.
    /// 
    /// This class should be added to a prefab that is then set as
    /// the Maze Tile Prefab in the MazeDrawer.
    /// 
    /// The MazeTile should have four walls as GameObjects, that
    /// can be turned off individually.
    /// </summary>
    public class MazeTile : MonoBehaviour 
    {
        #region Enums
        /// <summary>
        /// An enum to represent a direction in the maze.
        /// </summary>
        public enum Direction
        {
            North,
            South,
            East,
            West,
        }
        #endregion // Enums

        #region Inspector Variables
        /// <summary>
        /// A GameObject that represents the north wall of this tile.
        /// </summary>
        [SerializeField] private GameObject _northWallGfx;
        /// <summary>
        /// A GameObject that represents the south wall of this tile.
        /// </summary>
        [SerializeField] private GameObject _southWallGfx;
        /// <summary>
        /// A GameObject that represents the east wall of this tile.
        /// </summary>
        [SerializeField] private GameObject _eastWallGfx;
        /// <summary>
        /// A GameObject that represents the west wall of this tile.
        /// </summary>
        [SerializeField] private GameObject _westWallGfx;
        #endregion // Inspector Variables

        #region Public Properties
        /// <summary>
        /// Gets or sets the x position.
        /// </summary>
        /// <value>The x position.</value>
        public int PositionX { get; set; }

        /// <summary>
        /// Gets or sets the y position.
        /// </summary>
        /// <value>The y position.</value>
        public int PositionY { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this MazeTile 
        /// has been visited.
        /// </summary>
        /// <value><c>true</c> if visited; otherwise, <c>false</c>.</value>
        public bool Visited { get; set; }
        #endregion // Public Properties

        #region Public Methods
        /// <summary>
        /// Opens or closes a passage in the specified direction.
        /// </summary>
        /// <param name="direction">Direction.</param>
        /// <param name="open">If set to <c>true</c> open, else close.</param>
        public void OpenPassage(Direction direction, bool open = true)
        {
            switch (direction)
            {
                case Direction.North:
                    _northWallGfx.SetActive(!open);
                    break;
                case Direction.South:
                    _southWallGfx.SetActive(!open);
                    break;
                case Direction.East:
                    _eastWallGfx.SetActive(!open);
                    break;
                case Direction.West:
                    _westWallGfx.SetActive(!open);
                    break;
            }
        }
        #endregion // Public Methods
    }
}
