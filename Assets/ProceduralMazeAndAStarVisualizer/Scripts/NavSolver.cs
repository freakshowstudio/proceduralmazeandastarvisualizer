﻿
/*! 
 * \file
 * \author Stig Olavsen <stig.olavsen@freakshowstudio.com>
 * \author http://www.freakshowstudio.com
 * \date © 2016
 * \copyright Unlicense <http://unlicense.org/>
 */

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Maze
{
    /// <summary>
    /// A class that solves for a path through the maze using the
    /// A* algorithm.
    /// 
    /// The algorithm will use the navigation graph created by the 
    /// MazeGenerator.
    /// 
    /// For a description of the A* algorithm, see this link: 
    /// https://en.wikipedia.org/wiki/A*_search_algorithm
    /// 
    /// The class is implemented as an IEnumerator, which allows us to run
    /// it as a coroutine in Unity.
    /// </summary>
    public class NavSolver : IEnumerator
    {
        #region Delegates
        /// <summary>
        /// A delegate defining a heuristic method to be used by the solver.
        /// </summary>
        public delegate float CalculateHeuristic(NavNode start, NavNode end);
        #endregion // Delegates

        #region Private Variables
        /// <summary>
        /// A binary heap used to store all the open nodes in the graph.
        /// </summary>
        private BinaryHeap<NavNode> _openNodes;
        /// <summary>
        /// The heuristic method.
        /// </summary>
        private CalculateHeuristic _heuristicMethod;
        /// <summary>
        /// Set once the algorithm finishes, true if a path was found.
        /// </summary>
        private bool _didFindPath = false;
        /// <summary>
        /// The resulting path once the algorithm finishes.
        /// </summary>
        private LinkedList<NavNode> _path;
        /// <summary>
        /// The delay.
        /// </summary>
        private WaitForSecondsEnumerator _delay;
        /// <summary>
        /// The start node.
        /// </summary>
        private NavNode _start;
        /// <summary>
        /// The end node.
        /// </summary>
        private NavNode _end;
        /// <summary>
        /// The cursor.
        /// </summary>
        private Cursor _cursor;
        /// <summary>
        /// The breadcrumb drawer.
        /// </summary>
        private NavBreadcrumbDrawer _breadcrumbDrawer;
        #endregion // Private Variables


        #region Public Properties
        /// <summary>
        /// Gets a value indicating whether a path was found.
        /// </summary>
        /// <value><c>true</c> if path found; otherwise, <c>false</c>.</value>
        public bool PathFound
        {
            get { return _didFindPath; }
        }

        /// <summary>
        /// Gets the path.
        /// </summary>
        /// <value>The path.</value>
        public LinkedList<NavNode> Path
        {
            get { return _path; }
        }
        #endregion // Public Properties

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the NavSolver class.
        /// </summary>
        /// <param name="heuristicMethod">Heuristic method.</param>
        /// <param name="start">Start node.</param>
        /// <param name="end">End node.</param>
        /// <param name="delay">Delay.</param>
        /// <param name="size">Size.</param>
        /// <param name="cursor">Cursor.</param>
        /// <param name="breadcrumbDrawer">Breadcrumb drawer.</param>
        public NavSolver(
            CalculateHeuristic heuristicMethod, 
            NavNode start, NavNode end, 
            float delay = 1f, int size=200,
            Cursor cursor = null,
            NavBreadcrumbDrawer breadcrumbDrawer = null)
        {
            _breadcrumbDrawer = breadcrumbDrawer;
            _heuristicMethod = heuristicMethod;
            _delay = new WaitForSecondsEnumerator(delay);
            _didFindPath = false;
            _path = new LinkedList<NavNode>();
            _openNodes = new BinaryHeap<NavNode>(size);
            _start = start;
            _end = end;
            _cursor = cursor;
            Reset();
        }
        #endregion // Constructors

        #region IEnumerator Implementation
        /// <summary>
        /// A single coroutine step for Unity.
        /// </summary>
        /// <returns><c>true</c>, if the coroutine should continue running, 
        /// <c>false</c> otherwise.</returns>
        public bool MoveNext()
        {
            return SolveStep();
        }

        /// <summary>
        /// Reset this instance.
        /// </summary>
        public void Reset()
        {
            _didFindPath = false;
            _path.Clear();
            _openNodes.Clear();
            _openNodes.Insert(_start);
            if (_breadcrumbDrawer != null)
            {
                _breadcrumbDrawer.Clear();
            }
        }

        /// <summary>
        /// Delay between execution steps.
        /// 
        /// An object in Current property, will be processed by Unity's 
        /// coroutine scheduler after executing MoveNext() method.
        /// </summary>
        /// <value>The delay.</value>
        public object Current
        {
            get
            {
                _delay.Reset();
                return _delay;
            }
        }
        #endregion // IEnumerator Implementation

        #region Private Methods
        /// <summary>
        /// A single step in solving for a path.
        /// </summary>
        /// <returns><c>true</c>, if we should continue solving, 
        /// <c>false</c> otherwise.</returns>
        private bool SolveStep()
        {
            if (_openNodes.Count < 1)
            {
                // No path found
                _didFindPath = false;
                _path.Clear();
                return false;
            }

            // The next node to examine
            NavNode node = _openNodes.Pop();

            UpdateCursor(node);
            DropBreadcrumb(node);

            if (node.Equals(_end))
            {
                // We found a path
                _didFindPath = true;
                TracePath(_end, ref _path);
                return false;
            }

            // Close the current node
            node.Closed = true;

            // Examine all the edges of this node
            foreach (NavEdge edge in node.Edges)
            {
                if (edge.ToNode.Closed)
                {
                    continue;
                }

                // If an edge is in the open nodes set
                if (_openNodes.Contains(edge.ToNode))
                {
                    // Check if the neighbour can be reached faster through
                    // this node, than through the previously calculated 
                    // path
                    float alternateDistanceCost = 
                        node.DistanceCost + edge.Weight;
                    
                    if (edge.ToNode.DistanceCost > alternateDistanceCost)
                    {
                        // Update the neighbour with the new cost
                        edge.ToNode.DistanceCost = alternateDistanceCost;
                        edge.ToNode.Parent = node;
                    }
                    continue;
                }

                // Edge is not in open nodes, so calculate its costs 
                // and add it to the open nodes
                edge.ToNode.Parent = node;
                edge.ToNode.DistanceCost = 
                    node.DistanceCost + edge.Weight;
                edge.ToNode.HeuristicCost = 
                    _heuristicMethod(edge.ToNode, _end);
                _openNodes.Insert(edge.ToNode);
            }

            return true;
        }
            
        /// <summary>
        /// Updates the cursor.
        /// </summary>
        /// <param name="node">Node.</param>
        private void UpdateCursor(NavNode node)
        {
            if (_cursor != null)
            {
                _cursor.SetPosition(
                    new Vector2(node.PositionX, node.PositionY));
            }
        }

        /// <summary>
        /// Drops a breadcrumb.
        /// </summary>
        /// <param name="node">Node.</param>
        private void DropBreadcrumb(NavNode node)
        {
            if (_breadcrumbDrawer != null)
            {
                _breadcrumbDrawer.DropBreadcrumb(node);
            }
        }
        #endregion Private Methods

        #region Private Static Functions
        /// <summary>
        /// Traces a path.
        /// 
        /// Given an end NavNode, this will trace back to the start, and
        /// calculate a path from the start to the end as a LinkedList.
        /// </summary>
        /// <param name="end">End.</param>
        /// <param name="path">Path.</param>
        private static void TracePath(
            NavNode end, ref LinkedList<NavNode> path)
        {
            path.Clear();
            NavNode node = end;
            while (node.Parent != null)
            {
                path.AddFirst(node);
                node = node.Parent;
            }
            path.AddFirst(node);
        }
        #endregion // Private Static Functions
    }
}

