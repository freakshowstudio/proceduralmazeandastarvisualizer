﻿
/*! 
 * \file
 * \author Stig Olavsen <stig.olavsen@freakshowstudio.com>
 * \author http://www.freakshowstudio.com
 * \date © 2016
 * \copyright Unlicense <http://unlicense.org/>
 */

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Maze
{
    /// <summary>
    /// A class that will handle drawing the final path, as found by
    /// the NavSolver.
    /// 
    /// If a path is found (it always should in our case), this class
    /// will create a number of LineRenderer objects that are made to look
    /// like arrows, that will point the way between each node in the path.
    /// </summary>
    public class NavPathDrawer
    {
        #region Private Variables
        /// <summary>
        /// The Z position of the path.
        /// </summary>
        private float _height;
        /// <summary>
        /// The start width of the LineRenderer.
        /// </summary>
        private float _startWidth;
        /// <summary>
        /// The end width of the LineRenderer.
        /// </summary>
        private float _endWidth;
        /// <summary>
        /// The layer that the path should use.
        /// </summary>
        private int _layer;
        /// <summary>
        /// The material used by the LineRenderer.
        /// </summary>
        private Material _material;
        /// <summary>
        /// A list that keeps track of the drawn lines. Used to be able to
        /// destroy the lines when needed.
        /// </summary>
        private List<GameObject> _segments;
        /// <summary>
        /// A Transform that the lines should be parented to.
        /// </summary>
        private Transform _parent;
        #endregion // Private Variables

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the NavPathDrawer class.
        /// </summary>
        /// <param name="startWidth">Start width.</param>
        /// <param name="endWidth">End width.</param>
        /// <param name="material">Material.</param>
        /// <param name="height">Height.</param>
        /// <param name="layer">Layer.</param>
        /// <param name="parent">Parent.</param>
        public NavPathDrawer(
            float startWidth, float endWidth, 
            Material material, 
            float height = 0f, 
            int layer = 0,
            Transform parent = null)
        {
            _startWidth = startWidth;
            _endWidth = endWidth;
            _material = material;
            _height = height;
            _layer = layer;
            _segments = new List<GameObject>();
            if (parent == null)
            {
                GameObject parentGO = new GameObject("Navigation Path");
                parent = parentGO.transform;
            }
            _parent = parent;
        }
        #endregion // Constructors


        #region Public Methods
        /// <summary>
        /// Draws the given path.
        /// </summary>
        /// <param name="path">Path.</param>
        public void DrawPath(LinkedList<NavNode> path)
        {
            LinkedListNode<NavNode> node = path.First;
            while (node.Next != null)
            {
                DrawPathSegment(node.Value, node.Next.Value);
                node = node.Next;
            }
        }

        /// <summary>
        /// Destroys all the created lines.
        /// </summary>
        public void Clear()
        {
            foreach (GameObject segment in _segments)
            {
                GameObject.Destroy(segment);
            }
            _segments.Clear();
        }
        #endregion // Public Methods

        #region Private Methods
        /// <summary>
        /// Draws a path segment.
        /// </summary>
        /// <param name="from">From.</param>
        /// <param name="to">To.</param>
        private void DrawPathSegment(NavNode from, NavNode to)
        {
            Vector3 startPosition = 
                new Vector3(from.PositionX, from.PositionY, _height);
            Vector3 endPosition = 
                new Vector3(to.PositionX, to.PositionY, _height);
            Vector3 halfwayPosition = 
                startPosition + ((endPosition - startPosition) * 0.75f);

            string segmentName = 
                string.Format("Path segment from {0} to {1}", from, to);
            
            GameObject segmentObject = new GameObject(segmentName);
            segmentObject.transform.SetParent(_parent);
            segmentObject.layer = _layer;

            LineRenderer segmentLine = 
                segmentObject.AddComponent<LineRenderer>();
            segmentLine.SetPositions(
                new Vector3[] { startPosition, halfwayPosition });
            segmentLine.startWidth = _startWidth;
            segmentLine.endWidth = _endWidth;
            segmentLine.material = _material;
            _segments.Add(segmentObject);
        }
        #endregion // Private Methods
    }
}

