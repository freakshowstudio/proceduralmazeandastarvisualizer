﻿
/*! 
 * \file
 * \author Stig Olavsen <stig.olavsen@freakshowstudio.com>
 * \author http://www.freakshowstudio.com
 * \date © 2016
 * \copyright Unlicense <http://unlicense.org/>
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Maze
{
    /// <summary>
    /// The runtime user interface for the MazeDrawer.
    /// </summary>
    public class MazeUI : MonoBehaviour 
    {
        #region Inspector Variables
        /// <summary>
        /// A reference to the maze drawer.
        /// </summary>
        [SerializeField] private MazeDrawer _mazeDrawer;
        /// <summary>
        /// The main camera.
        /// </summary>
        [SerializeField] private Camera _mainCamera;
        /// <summary>
        /// The minimize button.
        /// </summary>
        [SerializeField] private Button _minimizeButton;
        /// <summary>
        /// The width input.
        /// </summary>
        [SerializeField] private InputField _widthInput;
        /// <summary>
        /// The height input.
        /// </summary>
        [SerializeField] private InputField _heightInput;
        /// <summary>
        /// The heuristic dropdown.
        /// </summary>
        [SerializeField] private Dropdown _heuristicDropdown;
        /// <summary>
        /// The seed input.
        /// </summary>
        [SerializeField] private InputField _seedInput;
        /// <summary>
        /// The randomize seed button.
        /// </summary>
        [SerializeField] private Button _randomizeSeedButton;
        /// <summary>
        /// The delay input.
        /// </summary>
        [SerializeField] private InputField _delayInput;
        /// <summary>
        /// The start button.
        /// </summary>
        [SerializeField] private Button _startButton;
        /// <summary>
        /// The start button text.
        /// </summary>
        [SerializeField] private Text _startButtonText;
        /// <summary>
        /// The status text.
        /// </summary>
        [SerializeField] private Text _statusText;
        #endregion // Inspector Variables

        #region Private Variables
        /// <summary>
        /// If UI panel is currently minimized.
        /// </summary>
        private bool _isMinimized = false;
        /// <summary>
        /// If the maze is currently generating.
        /// </summary>
        private bool _isGenerating = false;
        #endregion // Private Variables

        #region Unity Methods
        /// <summary>
        /// Unity Start method.
        /// </summary>
        void Start()
        {
            if (_mainCamera == null)
            {
                _mainCamera = Camera.main;
            }

            _widthInput.text = _mazeDrawer.Width.ToString();
            _heightInput.text = _mazeDrawer.Height.ToString();
            _seedInput.text = _mazeDrawer.Seed.ToString();
            _delayInput.text = _mazeDrawer.Delay.ToString();
            _statusText.text = "Waiting...";

            _mazeDrawer.onFinishedGenerating.AddListener(delegate()
            {
                StopGenerating();
            });

            _minimizeButton.onClick.AddListener(delegate()
            {
                TogglePanel();
            });

            _widthInput.onEndEdit.AddListener(delegate(string arg0)
            {
                int value;
                if (!int.TryParse(arg0, out value))
                {
                    value = 1;
                }
                value = Mathf.Max(1, value);
                _mazeDrawer.Width = value;
                _widthInput.text = value.ToString();
            });

            _heightInput.onEndEdit.AddListener(delegate(string arg0)
            {
                int value;
                if (!int.TryParse(arg0, out value))
                {
                    value = 1;
                }
                value = Mathf.Max(1, value);
                _mazeDrawer.Height = value;
                _heightInput.text = value.ToString();
            });

            _heuristicDropdown.onValueChanged.AddListener(delegate(int arg0)
            {
                NavHeuristic.Method method = (NavHeuristic.Method) arg0;
                _mazeDrawer.Heuristic = method;
            });

            _seedInput.onEndEdit.AddListener(delegate(string arg0)
            {
                int value;
                if (!int.TryParse(arg0, out value))
                {
                    value = 0;
                }
                _mazeDrawer.Seed = value;
                _seedInput.text = value.ToString();
            });

            _randomizeSeedButton.onClick.AddListener(delegate()
            {
                int value = 
                    UnityEngine.Random.Range(int.MinValue, int.MaxValue);
                _mazeDrawer.Seed = value;
                _seedInput.text = value.ToString();
            });

            _delayInput.onEndEdit.AddListener(delegate(string arg0)
            {
                float value;
                if (!float.TryParse(arg0, out value))
                {
                    value = 0.1f;
                }
                value = Mathf.Max(0f, value);
                _mazeDrawer.Delay = value;
                _delayInput.text = value.ToString();
            });

            _startButton.onClick.AddListener(delegate()
            {
                OnStartButton();
            });
        }
        #endregion // Unity Methods

        #region Private Methods
        /// <summary>
        /// Toggles the panel.
        /// </summary>
        private void TogglePanel()
        {
            RectTransform rectTransform = (RectTransform) transform;
            if (_isMinimized)
            {
                rectTransform.sizeDelta = 
                    new Vector2(rectTransform.sizeDelta.x, 720f);
                _isMinimized = false;
            }
            else
            {
                rectTransform.sizeDelta = 
                    new Vector2(rectTransform.sizeDelta.x, 45f);
                _isMinimized = true;
            }
        }

        /// <summary>
        /// Handle user clicking on the start button
        /// </summary>
        private void OnStartButton()
        {
            if (!_isGenerating)
            {
                StartGenerating();
            }
            else
            {
                StopGenerating();
            }
        }

        /// <summary>
        /// Starts generating the maze.
        /// </summary>
        private void StartGenerating()
        {
            
            _isGenerating = true;
            DisableControls();
            RepositionCamera();
            _mazeDrawer.StartGenerating();
        }

        /// <summary>
        /// Stops (interrupts) generating the maze.
        /// </summary>
        private void StopGenerating()
        {
            _mazeDrawer.StopGenerating();
            EnableControls();
            _isGenerating = false;
        }

        /// <summary>
        /// Repositions the camera.
        /// </summary>
        private void RepositionCamera()
        {
            int mazeWidth = _mazeDrawer.Width;
            int mazeHeight = _mazeDrawer.Height;

            float mazeAspect = (float) mazeWidth / mazeHeight;
            float cameraAspect = _mainCamera.aspect;

            if (mazeAspect <= cameraAspect)
            {
                _mainCamera.orthographicSize = (mazeHeight / 2f) + 1f;
            }
            else
            {
                float cameraHeight = (float) mazeWidth / cameraAspect;
                _mainCamera.orthographicSize = (cameraHeight / 2f) + 1f;
            }

            float px = (mazeWidth / 2f) - 0.5f;
            float py = (mazeHeight / 2f) - 0.5f;
            float pz = -10f;

            _mainCamera.transform.position = new Vector3(px, py, pz);
        }

        /// <summary>
        /// Disables the controls.
        /// </summary>
        private void DisableControls()
        {
            _widthInput.interactable = false;
            _heightInput.interactable = false;
            _heuristicDropdown.interactable = false;
            _seedInput.interactable = false;
            _randomizeSeedButton.interactable = false;
            _delayInput.interactable = false;
            _statusText.text = "Generating...";
            _startButtonText.text = "Stop";
        }

        /// <summary>
        /// Enables the controls.
        /// </summary>
        private void EnableControls()
        {
            _widthInput.interactable = true;
            _heightInput.interactable = true;
            _heuristicDropdown.interactable = true;
            _seedInput.interactable = true;
            _randomizeSeedButton.interactable = true;
            _delayInput.interactable = true;
            _statusText.text = "Waiting...";
            _startButtonText.text = "Start";
        }
        #endregion // Private Methods
	}
}
