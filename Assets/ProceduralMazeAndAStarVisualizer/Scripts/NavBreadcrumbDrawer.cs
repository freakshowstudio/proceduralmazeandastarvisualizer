﻿
/*! 
 * \file
 * \author Stig Olavsen <stig.olavsen@freakshowstudio.com>
 * \author http://www.freakshowstudio.com
 * \date © 2016
 * \copyright Unlicense <http://unlicense.org/>
 */

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace Maze
{
    /// <summary>
    /// A class that will handle drawing of navigation breadcrumbs.
    /// 
    /// Breadcrumbs are used by the NavSolver to visualize each visited
    /// node, and display the distance cost, heuristic cost, and total cost
    /// of each visited node.
    /// </summary>
    public class NavBreadcrumbDrawer
    {
        #region Private Variables
        /// <summary>
        /// The breadcrumb prefab.
        /// </summary>
        private GameObject _prefab;
        /// <summary>
        /// A list of instantiated breadcrumbs. Used so the breadcrumbs can
        /// be deleted when necessary.
        /// </summary>
        private List<GameObject> _breadcrumbs;
        /// <summary>
        /// A Transform that should act as the parent for the breadcrumbs.
        /// </summary>
        private Transform _parent;
        #endregion // Private Variables

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the NavBreadcrumbDrawer class.
        /// </summary>
        /// <param name="prefab">The breadcrumb prefab.</param>
        /// <param name="parent">The parent Transform.</param>
        public NavBreadcrumbDrawer(
            GameObject prefab,
            Transform parent = null)
        {
            _prefab = prefab;
            _breadcrumbs = new List<GameObject>();
            if (parent == null)
            {
                GameObject parentGO = new GameObject("Navigation Breadcrumbs");
                parent = parentGO.transform;
            }
            _parent = parent;
        }
        #endregion // Constructors

        #region Public Methods
        /// <summary>
        /// Drops a breadcrumb on a given node.
        /// </summary>
        /// <param name="node">Node where to drop the breadcrumb.</param>
        public void DropBreadcrumb(NavNode node)
        {
            GameObject breadcrumbObject = 
                GameObject.Instantiate<GameObject>(
                    _prefab, 
                    new Vector2(node.PositionX, node.PositionY), 
                    Quaternion.identity);
            breadcrumbObject.transform.SetParent(_parent);
            Breadcrumb breadcrumb = 
                breadcrumbObject.GetComponent<Breadcrumb>();
            Assert.IsNotNull(breadcrumb);
            breadcrumb.SetCost(
                node.DistanceCost, node.HeuristicCost, node.TotalCost);
            _breadcrumbs.Add(breadcrumbObject);
        }

        /// <summary>
        /// Destroys all the instantiated breadcrumbs.
        /// </summary>
        public void Clear()
        {
            foreach (GameObject breadcrumb in _breadcrumbs)
            {
                GameObject.Destroy(breadcrumb);
            }
            _breadcrumbs.Clear();
        }
        #endregion // Public Methods
    }
}

