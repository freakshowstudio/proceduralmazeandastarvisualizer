﻿
/*! 
 * \file
 * \author Stig Olavsen <stig.olavsen@freakshowstudio.com>
 * \author http://www.freakshowstudio.com
 * \date © 2016
 * \copyright Unlicense <http://unlicense.org/>
 */

using System;
using System.Collections;
using UnityEngine;

namespace Maze
{
    /// <summary>
    /// A class similar to Unitys WaitForSeconds, but implemented
    /// with the IEnumerator interface as opposed to CustomYieldInstruction.
    /// 
    /// This allows the class to be returned from the Current method of 
    /// another IEnumerator, which is useful when implementing custom
    /// Coroutines in Unity.
    /// 
    /// To re-use the same object of this class, it is necessary to call
    /// the Reset method, to wait for a new period of time.
    /// 
    /// See the Unity documentation for more information about custom
    /// coroutines:
    /// https://docs.unity3d.com/ScriptReference/CustomYieldInstruction.html
    /// </summary>
    public class WaitForSecondsEnumerator : IEnumerator
    {
        #region Private Variables
        /// <summary>
        /// The delay represents the time this coroutine should wait.
        /// </summary>
        private float _delay;
        /// <summary>
        /// The timer keeps track of the time when this coroutine was started.
        /// </summary>
        private float _timer;
        #endregion // Private Variables

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the WaitForSecondsEnumerator class.
        /// </summary>
        /// <param name="delay">The time to wait.</param>
        public WaitForSecondsEnumerator(float delay)
        {
            _delay = delay;
            _timer = Time.time;
        }
        #endregion // Constructors

        #region IEnumerator Implementation
        /// <summary>
        /// Next iteration of the IEnumerator.
        /// </summary>
        /// <returns><c>true</c>, if we should continue to wait, 
        /// <c>false</c> to stop waiting.</returns>
        public bool MoveNext()
        {
            return Time.time < _timer + _delay;
        }

        /// <summary>
        /// Reset the timer.
        /// </summary>
        public void Reset()
        {
            _timer = Time.time;
        }

        /// <summary>
        /// An object in Current property, will be processed by Unity's 
        /// coroutine scheduler after executing MoveNext() method.
        /// </summary>
        /// <value>Always returns null.</value>
        public object Current
        {
            get
            {
                return null;
            }
        }
        #endregion // IEnumerator Implementation
    }
}

