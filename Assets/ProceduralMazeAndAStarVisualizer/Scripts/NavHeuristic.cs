﻿
/*! 
 * \file
 * \author Stig Olavsen <stig.olavsen@freakshowstudio.com>
 * \author http://www.freakshowstudio.com
 * \date © 2016
 * \copyright Unlicense <http://unlicense.org/>
 */

using UnityEngine;

namespace Maze
{
    /// <summary>
    /// A static helper class for the heuristic functions used by the 
    /// MazeDrawer and NavSolver classes.
    /// 
    /// Contains an enum used to set the heuristic function used, and a
    /// helper function to get a callback to the correct function given a
    /// method.
    /// </summary>
    public static class NavHeuristic
    {
        #region Enums
        /// <summary>
        /// An enum used to represent a heuristic method.
        /// </summary>
        public enum Method
        {
            Manhattan,
            Linear,
        }
        #endregion // Enums

        #region Public Static Functions
        /// <summary>
        /// Gets a callback to a heuristic function, given a heuristic 
        /// method.
        /// </summary>
        /// <returns>A callback to a heuristic function.</returns>
        /// <param name="method">The wanted heuristic method.</param>
        public static NavSolver.CalculateHeuristic GetHeuristic(Method method)
        {
            switch (method)
            {
                case Method.Manhattan:
                    return ManhattanHeuristic;
                case Method.Linear:
                    return LinearHeuristic;
                default:
                    return ManhattanHeuristic;
            }
        }

        /// <summary>
        /// A manhattan heuristic function. 
        /// 
        /// This is the sum of the distance in the x and y directions, from
        /// NavNode a to NavNode b.
        /// 
        /// It is faster than the linear function, but slightly less precise.
        /// In practice though, the difference is negligle most of the time,
        /// and the reduced computational cost makes it more effective.
        /// </summary>
        /// <returns>The distance cost from a to b.</returns>
        /// <param name="a">The first NavNode.</param>
        /// <param name="b">The second NavNode.</param>
        public static float ManhattanHeuristic(NavNode a, NavNode b)
        {
            float dx = Mathf.Abs(b.PositionX - a.PositionX);
            float dy = Mathf.Abs(b.PositionY - a.PositionY);
            return dx + dy;
        }

        /// <summary>
        /// A linear heuristic function.
        /// 
        /// This is the linear distance between NavNode a and NavNode b,
        /// as given by Pythagoras theorem.
        /// 
        /// It is slightly slower than the Manhattan function, as it 
        /// uses a square root in the calculations, but is more accurate.
        /// </summary>
        /// <returns>The distance cost from a to b.</returns>
        /// <param name="a">The first NavNode.</param>
        /// <param name="b">The second NavNode.</param>
        public static float LinearHeuristic(NavNode a, NavNode b)
        {
            float dx = Mathf.Abs(b.PositionX - a.PositionX);
            float dy = Mathf.Abs(b.PositionY - a.PositionY);
            float d = Mathf.Sqrt((dx * dx) + (dy * dy));
            return d;
        }
        #endregion // Public Static Functions
    }
}

