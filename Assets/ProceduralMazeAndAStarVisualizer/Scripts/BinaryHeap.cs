﻿
/*! 
 * \file
 * \author Stig Olavsen <stig.olavsen@freakshowstudio.com>
 * \author http://www.freakshowstudio.com
 * \date © 2016
 * \copyright Unlicense <http://unlicense.org/>
 */

using System;
using System.Collections;
using System.Collections.Generic;

namespace Maze
{
    /// <summary>
    /// A Binary Heap datatype used for the priority queue of the A*
    /// algorithm.
    /// 
    /// Priority is determined by the IComparable interface.
    /// 
    /// See https://en.wikipedia.org/wiki/Binary_heap for an introduction
    /// to binary heaps.
    /// </summary>
    internal class BinaryHeap<T> where T : IComparable<T>
    {
        #region Private Variables
        /// <summary>
        /// A list of nodes that currently exists in the heap.
        /// </summary>
        private List<T> _nodes;
        #endregion // Private Variables

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the BinaryHeap class.
        /// </summary>
        /// <param name="capacity">Initial capacity.</param>
        public BinaryHeap(int capacity = 4)
        {
            _nodes = new List<T>(capacity);
        }
        #endregion // Constructors

        #region Properties
        /// <summary>
        /// Number of items in the heap.
        /// </summary>
        /// <value>The count.</value>
        public int Count
        {
            get { return _nodes.Count; }
        }
        #endregion // Properties

        #region Public Methods
        /// <summary>
        /// Insert the specified item into the heap.
        /// </summary>
        /// <param name="item">Item to insert.</param>
        public void Insert(T item)
        {
            _nodes.Add(item);
            HeapifyUp(Count-1);
        }

        /// <summary>
        /// Returns the topmost item in the heap without removing it.
        /// </summary>
        public T Peek()
        {
            return _nodes[0];
        }

        /// <summary>
        /// Removes and returns the topmost item in the heap.
        /// </summary>
        public T Pop()
        {
            T item = _nodes[0];
            _nodes[0] = _nodes[Count-1];
            _nodes.RemoveAt(Count - 1);
            HeapifyDown(0);
            return item;
        }

        /// <summary>
        /// Checks if the heap contains the specified item.
        /// </summary>
        /// <param name="item">Item.</param>
        public bool Contains(T item)
        {
            return _nodes.Contains(item);
        }

        /// <summary>
        /// Find the specified match.
        /// 
        /// Returns null if not found.
        /// </summary>
        /// <param name="match">Match.</param>
        public T Find(Predicate<T> match)
        {
            return _nodes.Find(match);
        }

        /// <summary>
        /// Clear this instance.
        /// </summary>
        public void Clear()
        {
            _nodes.Clear();
        }
        #endregion Public Methods

        #region Private Methods
        /// <summary>
        /// Heapifies up.
        /// </summary>
        /// <param name="idx">Index.</param>
        private void HeapifyUp(int idx)
        {
            if (idx <= 0)
            {
                return;
            }
            int parentIdx = (idx - 1) / 2;
            if (_nodes[idx].CompareTo(_nodes[parentIdx]) > 0)
            {
                Swap(idx, parentIdx);
                HeapifyUp(parentIdx);
            }
        }

        /// <summary>
        /// Heapifies down.
        /// </summary>
        /// <param name="idx">Index.</param>
        private void HeapifyDown(int idx)
        {
            int leftIdx = (idx * 2) + 1;
            int rightIdx = leftIdx + 1;
            int largestIdx = idx;

            if (leftIdx < Count && 
                _nodes[leftIdx].CompareTo(_nodes[largestIdx]) > 0)
            {
                largestIdx = leftIdx;
            }
            if (rightIdx < Count &&
                _nodes[rightIdx].CompareTo(_nodes[largestIdx]) > 0)
            {
                largestIdx = rightIdx;
            }
            if (largestIdx != idx)
            {
                Swap(idx, largestIdx);
                HeapifyDown(largestIdx);
            }
        }

        /// <summary>
        /// Swap the items with index a and b.
        /// </summary>
        /// <param name="a">The a component.</param>
        /// <param name="b">The b component.</param>
        private void Swap(int a, int b)
        {
            T t = _nodes[a];
            _nodes[a] = _nodes[b];
            _nodes[b] = t;
        }
        #endregion // Private Methods
	}
}
