﻿
/*! 
 * \file
 * \author Stig Olavsen <stig.olavsen@freakshowstudio.com>
 * \author http://www.freakshowstudio.com
 * \date © 2016
 * \copyright Unlicense <http://unlicense.org/>
 */

using System;

namespace Maze
{
    /// <summary>
    /// This class represents an edge between a NavNode and another.
    /// 
    /// As the MazeGenerator generates a maze, it will simultaneosly 
    /// create a navigation graph. This graph is what the NavSolver uses
    /// to find a path through the maze.
    /// 
    /// The NavEdge is a part of the navigation graph, and lets the NavSolver
    /// see which nodes are connected.
    /// </summary>
    public class NavEdge
    {
        #region Public Properties
        /// <summary>
        /// Gets or sets the from node of this NavEdge.
        /// </summary>
        /// <value>From node.</value>
        public NavNode FromNode { get; set; }
        /// <summary>
        /// Gets or sets the to node of this NavEdge.
        /// </summary>
        /// <value>To node.</value>
        public NavNode ToNode { get; set; }
        /// <summary>
        /// Gets or sets the weight of this NavEdge.
        /// </summary>
        /// <value>The weight.</value>
        public float Weight { get; set; }
        #endregion // Public Properties

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the NavEdge class.
        /// </summary>
        /// <param name="from">From NavNode.</param>
        /// <param name="to">To NavNode.</param>
        /// <param name="weight">Edge weight.</param>
        public NavEdge(NavNode from, NavNode to, float weight)
        {
            FromNode = from;
            ToNode = to;
            Weight = weight;
        }
        #endregion // Constructors

        #region Public Methods
        /// <summary>
        /// Returns a String that represents the current NavEdge.
        /// </summary>
        /// <returns>A String that represents the current NavEdge.</returns>
        public override string ToString()
        {
            return string.Format(
                "[NavEdge: FromNode={0}, ToNode={1}, Weight={2}]", 
                FromNode, ToNode, Weight);
        }
        #endregion // Public Methods
    }
}

