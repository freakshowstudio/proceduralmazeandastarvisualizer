﻿
/*! 
 * \file
 * \author Stig Olavsen <stig.olavsen@freakshowstudio.com>
 * \author http://www.freakshowstudio.com
 * \date © 2016
 * \copyright Unlicense <http://unlicense.org/>
 */

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

namespace Maze
{
    /// <summary>
    /// This is the main class that will draw a procedural maze, and 
    /// then find a path through the maze.
    /// </summary>
    public class MazeDrawer : MonoBehaviour 
    {
        #region Inspector Variables
        [Header("Maze Properties")]
        /// <summary>
        /// The random seed to use for the maze.
        /// </summary>
        [SerializeField] private int _seed = 0;
        /// <summary>
        /// The heuristic function to use in the A* solver.
        /// </summary>
        [SerializeField] private NavHeuristic.Method _heuristic;
        /// <summary>
        /// The width of the maze in tiles.
        /// </summary>
        [SerializeField] private int _width = 20;
        /// <summary>
        /// The height of the maze in tiles.
        /// </summary>
        [SerializeField] private int _height = 10;
        /// <summary>
        /// The delay between steps in generating/solving.
        /// </summary>
        [SerializeField] private float _updateDelay = 0.1f;
        /// <summary>
        /// The maze tile prefab.
        /// </summary>
        [SerializeField] private GameObject _mazeTilePrefab;

        [Header("Cursor")]
        /// <summary>
        /// The cursor prefab.
        /// </summary>
        [SerializeField] private GameObject _cursorPrefab;

        [Header("Breadcrumbs")]
        /// <summary>
        /// If breadcrumbs should be drawn.
        /// </summary>
        [SerializeField] private bool _drawBreadcrumbs = true;
        /// <summary>
        /// The breadcrumb prefab.
        /// </summary>
        [SerializeField] private GameObject _breadcrumbPrefab;

        [Header("Nav Graph Properties")]
        /// <summary>
        /// If the navigation graph should be drawn.
        /// </summary>
        [SerializeField] private bool _drawNavGraph = true;
        /// <summary>
        /// The navigation graph material.
        /// </summary>
        [SerializeField] private Material _navGraphMaterial;
        /// <summary>
        /// The start width of the navigation graph lines.
        /// </summary>
        [SerializeField] private float _navGraphStartWidth = 0.5f;
        /// <summary>
        /// The end width of the navigation graph lines.
        /// </summary>
        [SerializeField] private float _navGraphEndWidth = 0f;
        /// <summary>
        /// The layer to use for the navigation graph.
        /// </summary>
        [SerializeField] private int _navGraphLayer = 0;
        /// <summary>
        /// The height of the navigation graph (Z position).
        /// </summary>
        [SerializeField] private float _navGraphHeight = 0.1f;

        [Header("Path Properties")]
        /// <summary>
        /// If the final path should be drawn.
        /// </summary>
        [SerializeField] private bool _drawPath = true;
        /// <summary>
        /// The path material.
        /// </summary>
        [SerializeField] private Material _pathMaterial;
        /// <summary>
        /// The start width of the path lines.
        /// </summary>
        [SerializeField] private float _pathStartWidth = 0.6f;
        /// <summary>
        /// The end width of the path lines.
        /// </summary>
        [SerializeField] private float _pathEndWidth = 0f;
        /// <summary>
        /// The layer to use for the path.
        /// </summary>
        [SerializeField] private int _pathLayer = 0;
        /// <summary>
        /// The height of the path (Z position).
        /// </summary>
        [SerializeField] private float _pathHeight = 0.2f;
        #endregion // Inspector Variables

        #region Properties
        /// <summary>
        /// Gets or sets the seed.
        /// </summary>
        /// <value>The seed.</value>
        public int Seed
        {
            get { return _seed; }
            set { _seed = value; }
        }

        /// <summary>
        /// Gets or sets the update delay.
        /// </summary>
        /// <value>The update delay.</value>
        public float Delay
        {
            get { return _updateDelay; }
            set { _updateDelay = value; }
        }

        /// <summary>
        /// Gets or sets the width.
        /// </summary>
        /// <value>The width.</value>
        public int Width
        {
            get { return _width; }
            set { _width = value; }
        }

        /// <summary>
        /// Gets or sets the height.
        /// </summary>
        /// <value>The height.</value>
        public int Height
        {
            get { return _height; }
            set { _height = value; }
        }

        /// <summary>
        /// Gets or sets the heuristic function.
        /// </summary>
        /// <value>The heuristic function.</value>
        public NavHeuristic.Method Heuristic
        {
            get { return _heuristic; }
            set { _heuristic = value; }
        }
        #endregion // Properties

        #region Unity Events
        /// <summary>
        /// An event raised when the maze has finished generating.
        /// </summary>
        public UnityEvent onFinishedGenerating = new UnityEvent();
        #endregion // Unity Events

        #region Public Methods
        /// <summary>
        /// Starts generating and solving the maze.
        /// </summary>
        public void StartGenerating()
        {
            StartCoroutine(DrawAndSolveMaze());
        }

        /// <summary>
        /// Interrupts generating the maze.
        /// </summary>
        public void StopGenerating()
        {
            StopAllCoroutines();
        }
        #endregion // Public Methods

        #region Coroutines
        /// <summary>
        /// A coroutine that handles drawing and solving the maze.
        /// </summary>
        /// <returns>Coroutine IEnumerator.</returns>
        private IEnumerator DrawAndSolveMaze()
        {
            // First destroy any old maze we might have drawn
            while (transform.childCount > 0)
            {
                DestroyImmediate(transform.GetChild(0).gameObject);
            }

            // Set the seed for the randomizer
            UnityEngine.Random.InitState(_seed);

            // Create a cursor to use in visualization
            GameObject cursorObject = (GameObject) Instantiate(_cursorPrefab);
            Cursor cursor = cursorObject.GetComponent<Cursor>();
            cursor.transform.SetParent(transform);
            Assert.IsNotNull(cursor);

            // Create a navigation graph drawer helper, if the navigation 
            // graph should be drawn
            NavGraphDrawer navGraphDrawer = _drawNavGraph
                ? new NavGraphDrawer(
                    _navGraphStartWidth, _navGraphEndWidth, 
                    _navGraphMaterial, 
                    _navGraphHeight, 
                    _navGraphLayer,
                    transform)
                : null;

            // Create a breadcrumb drawer helper
            NavBreadcrumbDrawer breadcrumbDrawer = _drawBreadcrumbs
                ? new NavBreadcrumbDrawer(_breadcrumbPrefab, transform)
                : null;

            // Instantiate a maze generator
            MazeGenerator mazeGenerator = 
                new MazeGenerator(
                    _width, _height, 
                    _mazeTilePrefab, 
                    _updateDelay,  
                    cursor,
                    navGraphDrawer,
                    transform);

            // Run the maze generator as a coroutine
            yield return mazeGenerator;

            // Get the heuristic function that should be used in the
            // A* solver
            NavSolver.CalculateHeuristic heuristicDelegate =
                NavHeuristic.GetHeuristic(_heuristic);

            // Create a new A* solver 
            NavSolver pathSolver = 
                new NavSolver(
                    heuristicDelegate, 
                    mazeGenerator.NavGraph[0, 0], 
                    mazeGenerator.NavGraph[_width - 1, _height - 1], 
                    _updateDelay, 
                    _width * _height, 
                    cursor,
                    breadcrumbDrawer);

            // Run the A* solver as a coroutine
            yield return pathSolver;

            // Create a path drawer helper
            NavPathDrawer pathDrawer = 
                new NavPathDrawer(
                    _pathStartWidth, _pathEndWidth, 
                    _pathMaterial, 
                    _pathHeight, 
                    _pathLayer,
                    transform);

            // If the path should be drawn, and a path was found 
            // (in our case it always is), draw the final path
            if (_drawPath && pathSolver.PathFound)
            {
                pathDrawer.DrawPath(pathSolver.Path);
            }

            // Raise the finished generating event
            onFinishedGenerating.Invoke();
        }
        #endregion // Coroutines
    }
}
