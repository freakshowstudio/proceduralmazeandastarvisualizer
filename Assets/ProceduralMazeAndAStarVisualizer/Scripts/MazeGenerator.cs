﻿
/*! 
 * \file
 * \author Stig Olavsen <stig.olavsen@freakshowstudio.com>
 * \author http://www.freakshowstudio.com
 * \date © 2016
 * \copyright Unlicense <http://unlicense.org/>
 */

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace Maze
{
    /// <summary>
    /// This class will generate a random maze procedurally using a depth
    /// first algorithm.
    /// 
    /// The maze is initially filled with MazeTiles, which all have four 
    /// walls. It will then start at the lower left tile, and select a 
    /// random neighbouring tile, and remove the walls between them. The
    /// tile is marked as visited, and pushed on a stack. It will then move
    /// to the next tile, and again select a random neighbour, and continue
    /// in this fashion until it encounters a tile with no un-visited 
    /// neighbours. 
    /// 
    /// Once a tile with no un-visited neighbours is encountered, it will
    /// start popping elements of the stack, effectively moving backwards
    /// through the most recent tiles, until it again reaches a cell with
    /// un-visited neighbours. It will then do the same procedure from this 
    /// cell.
    /// 
    /// This continues until it again reaches the starting position, when
    /// the stack is empty. We can then be certain that all tiles have been
    /// visited.
    /// 
    /// The algorithm is chosen for its simplicity, it will tend to generate
    /// long corridors with a low number of branches. There will be no islands
    /// in the resulting maze, and every tile will be reachable from any other
    /// tile.
    /// 
    /// The class is implemented as an IEnumerator, which allows us to run
    /// it as a coroutine in Unity.
    /// </summary>
    public class MazeGenerator : IEnumerator
    {
        #region Private Variables
        /// <summary>
        /// A 2D array representing all the MazeTiles in the maze.
        /// </summary>
        private MazeTile[,] _graph;
        /// <summary>
        /// A navigation graph, used for pathfinding.
        /// </summary>
        private NavNode[,] _navGraph;
        /// <summary>
        /// The delay between the steps.
        /// </summary>
        private WaitForSecondsEnumerator _delayEnumerator;
        /// <summary>
        /// The cursor, used to indicate the current tile.
        /// </summary>
        private Cursor _cursor;
        /// <summary>
        /// A Transform representing the parent of the maze.
        /// </summary>
        private Transform _parent;
        /// <summary>
        /// The maze tile prefab.
        /// </summary>
        private GameObject _mazeTilePrefab;
        /// <summary>
        /// The stack.
        /// </summary>
        private Stack<MazeTile> _stack;
        /// <summary>
        /// The navigation graph drawer, used to visualize the resulting 
        /// navigation graph.
        /// </summary>
        private NavGraphDrawer _navGraphDrawer;
        #endregion // Private Variables

        #region Public Properties
        /// <summary>
        /// Gets the navigation graph.
        /// </summary>
        /// <value>The navigation graph.</value>
        public NavNode[,] NavGraph
        {
            get { return _navGraph; }
        }
        #endregion // Public Properties

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the MazeGenerator class.
        /// </summary>
        /// <param name="width">Width of the maze.</param>
        /// <param name="height">Height of the maze.</param>
        /// <param name="mazeTilePrefab">Maze tile prefab.</param>
        /// <param name="delay">Delay between steps.</param>
        /// <param name="cursor">Cursor.</param>
        /// <param name="navGraphDrawer">Navigation graph drawer.</param>
        /// <param name="parent">Parent.</param>
        public MazeGenerator(
            int width, int height, 
            GameObject mazeTilePrefab,
            float delay = 1f, 
            Cursor cursor = null,
            NavGraphDrawer navGraphDrawer = null,
            Transform parent = null)
        {
            _delayEnumerator = new WaitForSecondsEnumerator(delay);
            _cursor = cursor;
            _mazeTilePrefab = mazeTilePrefab;
            if (parent == null)
            {
                GameObject parentGO = new GameObject("Generated Maze");
                parent = parentGO.transform;
            }
            _parent = parent;
            _navGraphDrawer = navGraphDrawer;
            Assert.IsTrue(width > 0 && height > 0);
            InitializeMaze(width, height);
        }
        #endregion // Constructors

        #region IEnumerator Implementation
        /// <summary>
        /// A single coroutine step for Unity.
        /// </summary>
        /// <returns><c>true</c>, if the coroutine should continue running, 
        /// <c>false</c> otherwise.</returns>
        public bool MoveNext()
        {
            if (_stack.Count < 1)
            {
                return false;
            }

            CreateMazeStep();
            return true;
        }

        /// <summary>
        /// Reset this instance.
        /// </summary>
        public void Reset()
        {
            int width = _graph.GetLength(0);
            int height = _graph.GetLength(1);
            DestroyMaze();
            InitializeMaze(width, height);
        }

        /// <summary>
        /// Delay between execution steps.
        /// 
        /// An object in Current property, will be processed by Unity's 
        /// coroutine scheduler after executing MoveNext() method.
        /// </summary>
        /// <value>The delay.</value>
        public object Current
        {
            get
            {
                _delayEnumerator.Reset();
                return _delayEnumerator;
            }
        }
        #endregion // IEnumerator Implementation

        #region Private Methods
        /// <summary>
        /// Initializes the maze.
        /// 
        /// Creates a two dimensional maze of MazeTiles, with all walls up.
        /// </summary>
        /// <param name="width">Width of the maze.</param>
        /// <param name="height">Height of the maze.</param>
        private void InitializeMaze(int width, int height)
        {
            _graph = new MazeTile[width, height];
            _navGraph = new NavNode[width, height];

            for (int y = 0; y < height; ++y)
            {
                for (int x = 0; x < width; ++x)
                {                    
                    GameObject newTile = 
                        GameObject.Instantiate<GameObject>(
                            _mazeTilePrefab);
                    
                    newTile.transform.position = new Vector2(x, y);
                    newTile.transform.SetParent(_parent);

                    MazeTile newMazeTile = newTile.GetComponent<MazeTile>();
                    Assert.IsNotNull(newMazeTile);

                    _graph[x, y] = newMazeTile;
                    _graph[x, y].PositionX = x;
                    _graph[x, y].PositionY = y;

                    _navGraph[x, y] = new NavNode();
                    _navGraph[x, y].PositionX = x;
                    _navGraph[x, y].PositionY = y;
                }
            }
            _stack = new Stack<MazeTile>(width * height);
            _stack.Push(_graph[0, 0]);
        }

        /// <summary>
        /// Destroys the maze.
        /// </summary>
        private void DestroyMaze()
        {
            for (int y = 0; y < _graph.GetLength(1); ++y)
            {
                for (int x = 0; x < _graph.GetLength(0); ++x)
                {
                    GameObject.Destroy(_graph[x, y]);
                }
            }
            _stack.Clear();
        }

        /// <summary>
        /// A single step in creating the maze.
        /// 
        /// This will look at a single MazeTile for every time it is called,
        /// and update accordingly. 
        /// </summary>
        private void CreateMazeStep()
        {
            // Which MazeTile we should be looking at next
            MazeTile nextMazeTile = _stack.Peek();
            nextMazeTile.Visited = true;

            // Update the cursor
            if (_cursor != null)
            {
                _cursor.SetPosition(
                    new Vector2(
                        nextMazeTile.PositionX, 
                        nextMazeTile.PositionY));
            }

            // A list of neighbours to our current MazeTile
            List<MazeTile> neighbours = new List<MazeTile>(4);

            // Add a neighbour in each direction, if possible
            if ((nextMazeTile.PositionY + 1) < _graph.GetLength(1))
                neighbours.Add(
                    _graph[nextMazeTile.PositionX, 
                           nextMazeTile.PositionY + 1]);

            if ((nextMazeTile.PositionY - 1) >= 0)
                neighbours.Add(
                    _graph[nextMazeTile.PositionX, 
                           nextMazeTile.PositionY - 1]);

            if ((nextMazeTile.PositionX + 1) < _graph.GetLength(0))
                neighbours.Add(
                    _graph[nextMazeTile.PositionX + 1, 
                           nextMazeTile.PositionY]);

            if ((nextMazeTile.PositionX - 1) >= 0)
                neighbours.Add(
                    _graph[nextMazeTile.PositionX - 1, 
                           nextMazeTile.PositionY]);

            // Find the neighbours that have not been visited
            List<MazeTile> notVisitedNeighbours = 
                neighbours.FindAll(neighbour => neighbour.Visited == false);

            // Can we make a new connection to one or more neighbours?
            if (notVisitedNeighbours != null &&
                notVisitedNeighbours.Count > 0)
            {
                // Select a neighbour at random
                int randomNeighbourIndex = UnityEngine.Random.Range(
                    0, notVisitedNeighbours.Count);
                
                MazeTile nextNotVisitedNeighbour = 
                    notVisitedNeighbours[randomNeighbourIndex];

                // Create a new connection between tiles based on 
                // position indices.
                int x1 = nextMazeTile.PositionX;
                int x2 = nextNotVisitedNeighbour.PositionX;
                int y1 = nextMazeTile.PositionY;
                int y2 = nextNotVisitedNeighbour.PositionY;

                if (x1 == x2 && y1 < y2)
                {
                    nextMazeTile.OpenPassage(
                        MazeTile.Direction.North);
                    nextNotVisitedNeighbour.OpenPassage(
                        MazeTile.Direction.South);
                }
                if (x1 == x2 && y1 > y2)
                {
                    nextMazeTile.OpenPassage(
                        MazeTile.Direction.South);
                    nextNotVisitedNeighbour.OpenPassage(
                        MazeTile.Direction.North);
                }
                if (y1 == y2 && x1 > x2)
                {
                    nextMazeTile.OpenPassage(
                        MazeTile.Direction.West);
                    nextNotVisitedNeighbour.OpenPassage(
                        MazeTile.Direction.East);
                }
                if (y1 == y2 && x1 < x2)
                {
                    nextMazeTile.OpenPassage(
                        MazeTile.Direction.East);
                    nextNotVisitedNeighbour.OpenPassage(
                        MazeTile.Direction.West);
                }

                // Update navigation graph
                NavEdge edge = 
                    new NavEdge(_navGraph[x1, y1], _navGraph[x2, y2], 1f);
                _navGraph[x1, y1].Edges.Add(edge);
                _navGraph[x2, y2].Edges.Add(edge);

                if (_navGraphDrawer != null)
                {
                    _navGraphDrawer.DrawNavEdge(
                        _navGraph[x1, y1], _navGraph[x2, y2]);
                    _navGraphDrawer.DrawNavEdge(
                        _navGraph[x2, y2], _navGraph[x1, y1]);
                }

                // Move to this neighbour next
                _stack.Push(nextNotVisitedNeighbour);
                _cursor.SetStatus(Cursor.Status.Updating);
            }
            else
            {
                // No new connections can be made from here, so just
                // move to the next one.
                _stack.Pop();
                _cursor.SetStatus(Cursor.Status.Searching);
            }
        }
        #endregion //  Private Methods
    }
}

