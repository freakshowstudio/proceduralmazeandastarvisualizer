About
=====

This is a small Unity project that aims to demonstrate how to procedurally
generate a random maze, and then use A* pathfinding to find a route through
the maze, all while visualizing every step of the process.

A maze is generated with a simple depth first algorithm, simultaneously
also generating a navigation graph for the A* algorithm to use. Once the
maze is generated, the pathfinding algorithm will attempt to solve for
a path from the lower left corner to the upper right corner.

The project is not optimized for speed, but rather for simplicity and for
allowing a visualization of how the algorithms work.

A WebGL build of the project can be seen at
http://static.freakshowstudio.com/unity/maze/

Author
======

This software is released by Stig Olavsen of Freakshow Studio AS, the author
can be contacted by email at <stig.olavsen@freakshowstudio.com>


License
=======

This software is released under the UNLICENSE license, see the file UNLICENSE
for more information.